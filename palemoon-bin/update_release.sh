#!/bin/bash

wget https://www.palemoon.org/download.shtml
FILE=$(grep -n "gtk3.tar.xz" download.shtml)
SHA=$(head -n$(echo $(($(echo $FILE | cut -d':' -f1)+1))) download.shtml | tail -n1) # fast and ugly
VER=$(echo $FILE | awk -F'-' '{gsub(".linux", ""); print $2}')

rm download.shtml

source PKGBUILD

sed -i 's/'$pkgver'/'$VER'/g' PKGBUILD
sed -i 's/'$sha256sums_x86_64'/'$SHA'/g' PKGBUILD
