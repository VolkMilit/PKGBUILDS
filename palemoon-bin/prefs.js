pref("browser.ctrlTab.previews", false);
pref("browser.tabs.closeWiindowWithLastTab", false);
pref("browser.tabs.onTop", true);
pref("browser.tabs.warnOnClose", false);
pref("browser.tabs.warnOnCloseOtherTabs", false);

// PaleMoon recommends not changing this setting and big red letters in preferences appears, bla-bla
// We can't change OS, because modern sites will track you down not only by useragent, but analizing packages, but at least we
// can masque with Firefox instead of low-market PaleMoon
pref("network.http.useragent.global_override", "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0");
pref("general.useragent.override", "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0");

// That is suggested change from PaleMoon forums in order GitHub to work normally
pref("general.useragent.override.github.com", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:4.8) Goanna/20170101 PaleMoon/29.1.1");

pref("webgl.disabled", true);
pref("geo.enabled", false);

// Restore session by default
pref("browser.startup.page", 3);

// Replace stupid start.me page with about:newtab
pref("browser.newtab.url", "about:blank");
pref("browser.startup.homepage", "about:blank");

// Don't set titles on newtab page, I guess?
pref("browser.newtabpage.enabled", false);

// Disallow Js to disable some window elements
pref("dom.disable_window_open_feature.close", true);
pref("dom.disable_window_open_feature.menubar", true);
pref("dom.disable_window_open_feature.minimizable", true);
pref("dom.disable_window_open_feature.personalbar", true);
pref("dom.disable_window_open_feature.titlebar", true);
pref("dom.disable_window_open_feature.toolbar", true);

// Don't allow to open newwindow, istead, open new tab
pref("browser.link.open_newwindow.restriction", 0);

// Disable "Confirm you want to leave" dialog on page close
pref("dom.disable_beforeunload", true);

// Disable checking for default browser
pref("browser.defaultbrowser.notificationbar", false);
pref("browser.shell.skipDefaultBrowserCheck", true);
pref("browser.shell.skipDefaultBrowserCheckOnFirstRun", false);

// Disable PaleMoon addons blocklist
pref("extensions.blocklist.enabled", false); // Disable blocklist
pref("extensions.blocklist.interval", 0); // Never auto-update blocklist
