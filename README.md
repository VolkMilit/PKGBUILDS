# PKGBUILDS

Collection of unpopular\my modifications\your suggestion here packages for Arch\Artix Linux.

### palemoon-bin

[The statement about Pale Moon redistribution license and everything](https://gitlab.com/VolkMilit/PKGBUILDS/-/blob/master/palemoon-bin/README.MD).

Note: you need to add key to your keychain by execute ```gpg --keyserver=keyserver.ubuntu.com --recv-keys 8FCF9CEC``` (as a regular user).

PKGBUILD based on work of Bernhard Landauer and WorMzy Tykashi but with my additions.

- Add new profile selector (using Zenity, if any) in palemoon.desktop
- Remove all search engines, but DuckDuckGo
- duckduckgo.xml tracking querry was removed

Pref tweaked by default:

- Ctrl-Tab no longer display previews
- Don't close window with last tab
- Tabs always on top
- Disable warning on close
- Webgl was disabled
- Geolocation was disabled
- Don't check for default browser
- Display empty page on startup
- Display empty pages when press home button or new tab button
- Newtab page was disabled
- Disable all dom functions, that manipulates browser windows
- Don't open new windows when press links, instead open new tab
- Disable "comfirm you want to leave this page" (DOM future)

Useragent tweaks:

- Useragent by default is Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0

[official website](https://linux.palemoon.org/download/mainline/)

### python-androguard

Fix for https://aur.archlinux.org/packages/python-androguard, which was used wrong packages.

### zram-openrc

Start and stop zram module.

Spawn as many modules as many cores system has, and use total memory * 2 for total size of zram. For example: you have and 8 core CPU, like me and 16Gb of memory total. So your zram size will be 30Gb total (rougly 3.75Gb per module).

### How to build

cd in any directory, which you want to build. Run makepkg -si and you're done. Daemons automatically starts and add themeself to default runlevel.

### License

GPL v3.0
